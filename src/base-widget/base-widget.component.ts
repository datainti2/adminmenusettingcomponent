import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  template: '<p>admin-menu-setting works!</p>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
